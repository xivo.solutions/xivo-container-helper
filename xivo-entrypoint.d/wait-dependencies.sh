#!/bin/sh

PORT="${XIVO_HEALTH_CHECK_PORT:-2955}"


echo "Waiting for ependencies: ${DEPENDS_ON}" >&2 

OK=0
while [ "$OK" != 1 ]; do
	OK=1
	for s in $DEPENDS_ON; do
		if [ "`ncat --recv-only -w1 \"$s\" \"$PORT\"`" != OK ]; then
			echo "Service $s not ready" >&2
			sleep 1
			OK=0
			break
		fi
	done
done

echo "OK dependencies satisfied" >&2
