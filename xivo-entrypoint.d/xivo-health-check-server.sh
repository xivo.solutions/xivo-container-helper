#!/bin/sh

XIVO_HEALTH_CHECK_INTERNAL=/xivo-health-check-internal
PORT=${XIVO_HEALTCHECK_PORT:-2955}


if [ -f "$XIVO_HEALTH_CHECK_INTERNAL" -a -x "$XIVO_HEALTH_CHECK_INTERNAL" ]; then
	ncat -l -k -p "$PORT" -i 10ms -e "$XIVO_HEALTH_CHECK_INTERNAL" &
else
	ncat -l -k -p "$PORT" -i 10ms -c 'echo OK' &
fi
