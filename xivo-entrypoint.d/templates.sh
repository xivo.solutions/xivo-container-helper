
TEMPLATE_CONFIG=/etc/xivo-templates.conf


check_file() {
	[ -f "$1" ]
}

check_destination_file() {
	dir=`dirname "$1"`
	[ -d "$dir" ]
}


check_var() {
	echo "$1" | grep -qE '^[[:alnum:]_]+$'
}


in_filelist() {
	local file="$1"
	local filelist="$2"
	
	for f in $filelist; do
		if [ "$file" = "$f" ]; then
			return 0
		fi
	done
	
	return 1
}


is_file_placeholder() {
	local ph="$1"
	
	echo "$ph" | grep -q '^/'
}


is_env_placeholder() {
	local ph="$1"
	
	echo "$ph" | grep -Eq '^[[:alnum:]_]+(!!.*)?$'
}


get_file_content() {
	local file="$1"
	local filelist="$2"
	
	check_file "$file"
	
	if in_filelist "$file" "$filelist"; then
		echo "File include loop detected on file $file" >&2
		exit 1
	fi
	
	filelist="$filelist $file"
	proc_template "$file" /dev/stdout "$filelist"
}

replace_file_placeholder() {
	local file="$1"
	local filelist="$2"
	
	get_file_content "$file" "$filelist"
}

replace_env_placeholder() {
	local ph="$1"
	
	local var
	
	var=${ph%%!!*}
	default=${ph#*!!}
	if [ "$default" = "$ph" ]; then
		default=""
	fi
	
	check_var "$var"
	eval "content=\${$var=$default}"
	echo "$content"
}


proc_line() {
	local line="$1"
	local filelist="$2"
	
	local state=T
	local result=""
	
	while [ -n "$line" ]; do
		# copy literal text
		if [ "$state" = T ]; then
			txt="${line%%@@*}"
			if [ ${#txt} -eq ${#line} ]; then
				line=""
			else
				line=${line#*@@}
			fi
			result="${result}${txt}"
			state=R
		# replace placeholder
		else
			ph=${line%%@@*}
			if [ "$ph" = "$line" ]; then
				# closing @@ not found -> not a placeholder, just text
				result="${result}${line}"
				line=""
			else
				ph=${ph%@@}
				line=${line#*@@}
				# only file placeholder on line -> multiline
				if [ -z "$result" -a -z "$line" ] && is_file_placeholder "$ph"; then
					get_file_content "$ph" "$filelist"
				elif is_file_placeholder "$ph"; then
					rep=`replace_file_placeholder "$ph" "$filelist"`
					result="${result}${rep}"
				elif is_env_placeholder "$ph"; then
					rep=`replace_env_placeholder "$ph"`
					result="${result}${rep}"
				else
					echo Invalid placeholder "$ph" >&2
					exit 1
				fi
				state=T
			fi
		fi
	done
	echo "$result"
}

proc_template() {

    local input="$1"
    local output="$2"
    local filelist="$3"

    local line

    while true; do
    	    IFS='' read -r line
    	    if [ $? != 0 -a -z "$line" ]; then
    	    	break
    	    fi
    	    
            proc_line "$line" "$filelist"
    done <$input >>$output
}

if [ -n "$XCH_ALT_TEMPLATE_CONFIG" ]; then
	TEMPLATE_CONFIG="$XCH_ALT_TEMPLATE_CONFIG"
fi

if !  [ -f "$TEMPLATE_CONFIG" ]; then
	echo "File '$TEMPLATE_CONFIG' does not exist" >&2
else

	while true; do
		if ! read FILE; then
			[ -n "$FILE" ] || break
		fi

		if echo "$FILE" | grep -q "^[[:space:]]*$"; then
			continue
		fi

		if echo "$FILE" | grep -q "^[[:space:]]*#"; then
			continue
		fi

		TMPL="${FILE}.tmpl"
		DIR=`dirname "$FILE"`

		if ! check_file "$TMPL"; then
			echo "Template file $TMPL not found" >&2
			continue
		fi
		
		echo -n > "$FILE"
		proc_template "$TMPL" "$FILE"
		rm -- "$TMPL"
	done <$TEMPLATE_CONFIG

fi
