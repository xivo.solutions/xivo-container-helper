
(

ip2int() {
	IFS='.' read a b c d <<< $1
	
	echo "$(( ($a<<24) + ($b<<16) + ($c<<8) + $d ))"
}

mask2int() {
	local mask="$1"
	local shiftbits=$(( 32 - $mask ))
	
	echo "$(( (16#FFFFFFFF >> $shiftbits) << $shiftbits ))"
}

ip_eq() {
	local ip1="$1"
	local ip2="$2"
	
	[ `ip2int "$ip1"` -eq `ip2int "$ip2"` ]

}	
	


ip_is_bcast() {
	local ipint=`ip2int "$1"`
	local hostmaskint=$(( (~`mask2int $2`) & 16#FFFFFFFF ))
	
	[ $(( "$ipint" & $hostmaskint )) -eq $hostmaskint ] 
}


ip_is_net() {
	local ipint=`ip2int "$1"`
	local maskint=`mask2int $2`
	
	[ "$ipint" -eq $(( $ipint & $maskint )) ] 
}



ip_in_net() {
	local ipint=`ip2int "$1"`
	local netipint=`ip2int "$2"`
	local maskint=`mask2int "$3"`
	
	
	[ $(( $ipint & $maskint )) -eq $(( $netipint & $maskint )) ]
}


check_ip() {
	echo $1 | egrep -q '^((25[0-5])|(2[0-4][0-9])|(1[0-9][0-9])|([1-9][0-9])|(1?[0-9]))(\.((25[0-5])|(2[0-4][0-9])|(1[0-9][0-9])|([1-9][0-9])|(1?[0-9]))){3}$'
}

find_iface() {
    local ip="$1"
    local iface dummy iplist

    ip -4 -br addr show |
    while read iface dummy iplist; do
        if [ "$iface" = lo ]; then
                continue
        fi
        
        iface=${iface%%@*}
        
        for ifaddr in $iplist; do
            local ifip=`echo $ifaddr | cut -d / -f 1`
            local ifmask=`echo $ifaddr | cut -d / -f 2`
            
            if ip_in_net "$ip" "$ifip" "$ifmask"; then
                if ip_eq "$ip" "$ifip" ; then
                    echo "Trying to add IP '$ip' which already exists on interface '$iface'" >&2
                    return 1
                fi
                
                if ip_is_net "$ip" "$ifmask"; then
                    echo "Trying to add IP '$ip' which is network address on interface '$iface'" >&2
                    return 1
                fi
                
                if ip_is_bcast "$ip" "$ifmask"; then
                    echo "Trying to add IP '$ip' which is broadcast address on interface '$iface'" >&2
                    return 1
                fi
                
                echo "$iface $ifmask"
                return 0
            fi
    	done
    done

}	

if [ -z "$XCH_ADD_IP" ]; then
	exit 0
fi

if ! check_ip "$XCH_ADD_IP"; then
	echo "Invalid IP address '$XCH_ADD_IP'" >&2
	exit 1
fi

read iface mask <<< `find_iface "$XCH_ADD_IP"`
if [ -z "$iface" ]; then
	echo "Interface for address '$XCH_ADD_IP' not found" >&2
	exit 1
fi

if command -v capsh >/dev/null 2>&1 && ! capsh --has-p=cap_net_admin; then
	echo "No cap_net_admin capability, waiting for service update, sleep and exit" >&2
	sleep 30
	exit 1
fi

if ! ip addr add "${XCH_ADD_IP}/${mask}" dev "$iface"; then
	echo "Failed to add iface address ${XCH_ADD_IP}/${mask} to '$iface'" >&2
	exit 1
fi

)

[ $? -eq 0 ] || exit 1
