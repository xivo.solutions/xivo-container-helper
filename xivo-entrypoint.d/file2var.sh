
CONFIG_FILE=/etc/xivo-file2var.conf


check_file() {
	[ -f "$1" ]
}

check_var() {
	echo "$1" | grep -q '^[[:alpha:]_][[:alnum:]_]*$'
}



while true; do
	if ! read VAR FILE; then
		[ -n "$VAR" ] || break
	fi
	
        if echo "$VAR" | grep -q "^[[:space:]]*$"; then
                continue
        fi

        if echo "$VAR" | grep -q "^[[:space:]]*#"; then
                continue
        fi

	if ! check_file "$FILE"; then
		continue
	fi
	
	if ! check_var "$VAR"; then
		continue
	fi
	
	eval "export $VAR=`cat \"$FILE\"`"
done < $CONFIG_FILE
