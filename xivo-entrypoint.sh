#!/bin/bash

set -e

EPDIR=/xivo-entrypoint.d

check_user() {
	echo "$1" | grep -q '^[1-9][0-9]*$' && [ "$1" -lt 65536 ] || 
	id -u "$1" >/dev/null 2>&1
}
	

# Do not make the following loop a pipe consumer. Oherwise there will be 2 processes - produucer and consumer 
# and during exec only one (consumer) will be replaced. 


for F in  `find "${EPDIR}" -name \*.sh -executable | sort`; do
	echo "Starting entrypoint module $F" >&2
	if basename "$F" | grep -q "^99-lastexec"; then
		if [ -z "$XCH_RUN_AS" ]; then
			echo "EXEC entrypoint module " `basename $F` >&2
			exec "$F" $@
			echo exec FAILED >&2
			exit 1
		else
			if ! check_user "$XCH_RUN_AS"; then
				echo "Invalid user '$XCH_RUN_AS'" >&2
				exit 1
			else
				echo "EXEC entrypoint module " `basename $F` "as user $XCH_RUN_AS" >&2
				exec setpriv --reuid "$XCH_RUN_AS" -- "$F" "$@"
				echo exec FAILED >&2
				exit 1
			fi
		fi
	else
		echo "SOURCE entrypoint module $F" >&2
	  	if ! source "$F"; then
			echo "source FAILED" >&2
			exit 1
	  	fi
	fi
done
