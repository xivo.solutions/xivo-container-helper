# xivo-container-helper



## Name
xivo-container-helper

## Description
helpers to be installed to xivo containers. Currently only entrypoint scripts

### xivo-entrypoint.sh
Main entrypoint. Can be copied, moved or linked whenever you want
Runs scripts in /xivo-entrypoint.d/ directory in alphabetical order.
Scripts must have executable flag and name in form *.sh


### xivo-entrypoint.d/
Directory contains useful entrypoint scripts

#### template.sh
Reads configuration file /etc/xivo-templates.conf
Each line should have format:
template_file destination_file

Each template file is read, placeholders are replaced and result is stored
as a destination_file

Placeholder can be
- environment variable placeholde in format @@NAME@@
- file placeholder in format @@filename@@ where filename must be absolute path

If file placeholder is only text on line line breaks from source file are
preserved. Otherwise whole file is written to destination as a single line
Placeholders are replaced also in included file recursively.


#### Healt check and container dependency support
Consists of 2 entrypoint modules
- xivo-health-check-server.sh 
- wait-dependency.sh

* xivo-health-check-server.sh
Runs netcat listening on port 2955 and retrurns 1 line "OK" reponse when tcp connection is
  established.  If executable /xivo-healt-check-internal file exists, it is
run and stdout is sent back instead of default OK reponse.

* wait-dependency.sh
Expects list of services in env variable DEPENDS_ON. Tries to connect to
 port 2955 of all listed service names and checks if it returns OK.
 Check is repeated until all services return OK.

Both these modules require ncat sand ensure ncat installed accept and not
ignoire options -k -c -e -t --send-only


 









 

